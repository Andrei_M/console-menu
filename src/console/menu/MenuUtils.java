package console.menu;

public class MenuUtils {
    public static String indentationString = "\t";

    public static void print(String stringFormat, Object... args)
    {
        print(0, stringFormat, args);
    }

    public static void print(int indentationLevel, String stringFormat, Object... args)
    {
        print(indentationLevel, String.format(stringFormat, args));
    }

    public static void print(String str)
    {
        print(0, str);
    }

    public static void print(int indentationLevel, String str)
    {
        String indentation = repeat(indentationString, indentationLevel);
        System.out.println(indentation + str);
    }

    public static String repeat(String str, int countTimes)
    {
        String result = "";
        for (int i=0; i<countTimes; ++i) {
            result+= str;
        }
        return result;
    }
}
