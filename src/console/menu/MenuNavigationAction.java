package console.menu;

public enum MenuNavigationAction {
    None,
    GoBackOneStep,
    GoBackToFirstMenu,
    DisplayMenuAgain,
    ExitMenu,
}
