package console.menu;

import java.util.ArrayList;

public class MenuList extends ArrayList<MenuEntry> {
    private String title;
    private String message;

    public MenuList(String title)
    {
        this(title, "");
    }

    public MenuList(String title, String message)
    {
        this.title = title;
        this.message = message;
    }

    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return title; }

    public String getMessage() { return message; }
    public void setMessage(String message) { this.message = message; }
    
    @Override
    public String toString()
    {
        return title + ": " + message;
    }
}
