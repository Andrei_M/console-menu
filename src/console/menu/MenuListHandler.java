package console.menu;

import java.util.Scanner;

public class MenuListHandler{

    private static Scanner sc = new Scanner(System.in);

    public MenuNavigationAction displayMenuList(MenuList menuList) {
        MenuNavigationAction nextMenuAction = MenuNavigationAction.ExitMenu;
        do {
            MenuUtils.print("~~~~~~~~~~ " + menuList.getTitle() + " ~~~~~~~~~~");
            nextMenuAction = displayMenuList_helper(menuList);

            if (nextMenuAction == MenuNavigationAction.DisplayMenuAgain) {
                System.out.println();
            }
        } while (nextMenuAction==MenuNavigationAction.DisplayMenuAgain);

        if (nextMenuAction == MenuNavigationAction.GoBackOneStep)
        {
            MenuUtils.print("---------- Up one step we go ----------");
            nextMenuAction = MenuNavigationAction.DisplayMenuAgain;
        }

        return nextMenuAction;
    }

    private MenuNavigationAction displayMenuList_helper(MenuList menuList) {
        for (int i=0; i<menuList.size(); ++i) {
            displayMenuEntry(i, menuList.get(i));
        }
        System.out.print("Optiunea: ");
        int option = sc.nextInt();

        if (option >= 0 && option < menuList.size()) {
            MenuEntry chosenEntry = menuList.get(option);
            return chosenEntry.performAction();
        } else {
            MenuUtils.print("Oops!");
            return MenuNavigationAction.DisplayMenuAgain;
        }
    }

    private void displayMenuEntry(int entryNumber, MenuEntry menuEntry)
    {
        MenuUtils.print("%d. %s", entryNumber, menuEntry.toString());
    }


}
