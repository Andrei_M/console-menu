package console.menu;

public class MenuEntryHandler {

    protected MenuNavigationAction defaultMenuNavigationAction;

    public MenuEntryHandler()
    {
        this(MenuNavigationAction.DisplayMenuAgain);
    }

    public MenuEntryHandler(MenuNavigationAction menuNavigationAction)
    {
        this.defaultMenuNavigationAction = menuNavigationAction;
    }

    public void setDefaultMenuNavigationAction (MenuNavigationAction menuNavigationAction)
    { this.defaultMenuNavigationAction = menuNavigationAction; }
    public MenuNavigationAction getDefaultMenuNavigationAction() { return defaultMenuNavigationAction; }

    public MenuNavigationAction handleMenu(MenuEntry menuEntry)
    {
        return defaultMenuNavigationAction;
    }

    public static MenuEntryHandler goBack()
    {
        return new MenuEntryHandler(MenuNavigationAction.GoBackOneStep);
    }

    public static MenuEntryHandler exit()
    {
        return new MenuEntryHandler(MenuNavigationAction.ExitMenu);
    }
}
