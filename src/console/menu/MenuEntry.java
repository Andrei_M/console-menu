package console.menu;

public class MenuEntry {
    private String text;
    private MenuEntryHandler handler;

    public MenuEntry(String text)
    {
        this(text, null);
    }

    public MenuEntry(String text, MenuEntryHandler handler)
    {
        this.text = text;
        this.handler = handler;
    }

    public String getText() { return text; }
    public void setText(String menuText) { this.text = menuText; }

    public MenuEntryHandler getMenuHandler() { return handler; }
    public void setMenuHandler(MenuEntryHandler handler) { this.handler = handler; }

    public MenuNavigationAction performAction()
    {
        if (handler != null)
        {
            return handler.handleMenu(this);
        }

        return MenuNavigationAction.None;
    }
    
    @Override
    public String toString()
    {
        return text;
    }
}
